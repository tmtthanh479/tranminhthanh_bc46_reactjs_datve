import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import Result from "./Result";
import "./style.scss";

const BTDatve = () => {
  return (
    <div  >
      <div className="container mt-5">
        <h1>cinema</h1>
        <div className="row ">
          <div className="col-8">
            <h1 className="display-4">Đặt vé xem phim</h1>
            <div className="text-center p-3 font-weight-bold display-4 bg-dark text-white mt-3 ">SCREEN</div>
            <ChairList data={data} />
          </div>

          <div className="col-4">
            <Result />
          </div>
        </div>
      </div>
    </div>
  );
};

export default BTDatve;
