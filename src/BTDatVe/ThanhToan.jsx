import React from 'react'

const ThanhToan = () => {
  return (
    <div>
              <div>
        {/*   id="gioHang" của data-toggle="modal" data-target="#gioHang"  */}
        <div className="modal fade" id="gioHang" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-xl">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Chi tiết sản phẩm
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              {/* 2. tùy chỉnh body detail ở đây */}
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Tên SP</th>
                      <th>Hình Ảnh</th>
                      <th>Giá tiền</th>
                      <th>Số lượng</th>
                      <th>Tổng tiền</th>
                    </tr>
                  </thead>
                  <tbody>
                   
             
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ThanhToan