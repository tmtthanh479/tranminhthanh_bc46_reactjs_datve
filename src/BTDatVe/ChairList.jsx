import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  console.log(data);
  return <div>{data.map((hangGhe) => {

    return (
        <div className="d-flex mt-3 " style={{ gap: "10px" }}>
          <p style={{ width: "40px" }} className="text-center">
            {hangGhe.hang}
          </p>
          <div className="d-flex" style={{ gap: "10px" }}>
            {hangGhe.danhSachGhe.map((ghe) => {
              return <Chair ghe={ghe} />; 
            })}
          </div>
        </div>
      );
  })}</div>;
};

export default ChairList;
