import { HANDLE_CHAIR_BOOKING,PAY } from "./actionType";

export const chairBookingAction = (payload) => {
  return {
    type: HANDLE_CHAIR_BOOKING,
    payload,
  };
};
export const payAction = (payload) => {
  return{
    type: PAY,
    payload,
  }
}


