import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { baiTapDatveActions } from "../store/DatVe/slice";

const Result = () => {
  const { chairBookings } = useSelector((state) => state.btDatveToolkit);
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const handleThanhToan = () => {
    dispatch(baiTapDatveActions.setChairBooked()); // Corrected action name
    setShowModal(true);
  };
  return (
    <div>
      <h1>Danh sách ghê đang chọn</h1>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark bookingModel Chair">Ghế đã đặt</button>
        <button className="btn btn-outline-dark bookedModel Chair">Ghế đang đặt</button>
        <button className="btn btn-outline-dark">Ghế chưa đặt</button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Số Ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr key={ghe.soGhe}>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                <button
                  onClick={() => {
                    dispatch(baiTapDatveActions.setChairBooked(ghe));
                  }}
                  className="btn btn-danger"
                >
                  hủy
                </button>
              </td>
            </tr>
          ))}
          <tr>
            <td>tổng tiền</td>
            <td>{chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}</td>
          </tr>
        </tbody>
      </table>

      {/* <button onClick={()=>{
        dispatch(baiTapDatveActions.setChairBooked()) // type thì bắt buộc, payload có hay ko ko qt
      }}className="btn btn-success" >Thanh toán</button> */}
      <div>
        {/* ... Your existing code ... */}

        <button onClick={handleThanhToan} className="btn btn-success">
          xác Nhận
        </button>

        {/* Modal */}
        <div
          className={`modal ${showModal ? "show" : ""}`}
          tabIndex="-1"
          style={{ display: showModal ? "block" : "none" }}
        >
          <div
            className="modal-dialog modal-dialog-centered modal-lg" // Add modal-lg class here
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal Title</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => setShowModal(false)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                {/* Your modal body content */}
                <form>
                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <label htmlFor="inputEmail4">Tên</label>
                      <input type="text" className="form-control" />
                    </div>
                    <div className="form-group col-md-6">
                      <label htmlFor="inputPassword4">email</label>
                      <input type="email" className="form-control" />
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="inputAddress">Số điện thoại</label>
                    <input type="text" className="form-control" id="inputAddress" />
                  </div>
                  <div>
          
                  </div>

                  <div className="form-group">
                    <div className="form-check">
                      <input className="form-check-input" type="checkbox" id="gridCheck" />
                      <label className="form-check-label" htmlFor="gridCheck">
                        Tôi đã đọc và hiểu Điểu khoản và điều kiện.
                      </label>
                    </div>
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" onClick={() => setShowModal(false)}>
                  Hủy
                </button>
                <button type="button" className="btn btn-primary" onClick={() => setShowModal(false)}>
                  Thanh toán
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* End of Modal */}
      </div>
    </div>
  );
};

export default Result;
