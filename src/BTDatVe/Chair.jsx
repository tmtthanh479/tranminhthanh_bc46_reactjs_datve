import React from "react";
import cn from "classnames"; // thư viện
import { useDispatch, useSelector } from "react-redux";
import { baiTapDatveActions } from "../store/DatVe/slice";

const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  const { chairBookings,chairBooked } = useSelector((state) => state.btDatveToolkit);
  return (
    <div>
      <button
        onClick={() => {
          dispatch(baiTapDatveActions.setchairBookings(ghe));
        }}
        style={{ width: "50px" }}
        className={cn("btn btn-outline-secondary Chair", {
          booking: chairBookings.find((e) => e.soGhe === ghe.soGhe), 
          booked: chairBooked.find((e) => e.soGhe === ghe.soGhe), 

        })}
      >
        {ghe.soGhe}
      </button>
    </div>
  );
};

export default Chair;
