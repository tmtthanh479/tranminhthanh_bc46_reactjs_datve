import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  chairBookings: [],
  chairBooked: [],
};

const BTDatveSlinece = createSlice({
  name: "BTDatVe",
  initialState,
  reducers: {
    setchairBookings: (state, action) => {
      const index = state.chairBookings.findIndex((e) => e.soGhe === action.payload.soGhe);

      if (index !== -1) {
        state.chairBookings.splice(index, 1);
      } else {
        state.chairBookings.push(action.payload);
      }
    },
    setChairBooked: (state, { payload }) => {
      console.log();
      state.chairBooked = [...state.chairBooked, ...state.chairBookings];
      state.chairBookings = [];
    },
  },
});

export const { actions: baiTapDatveActions, reducer: baiTapDatveReducerToolkit } = BTDatveSlinece;
