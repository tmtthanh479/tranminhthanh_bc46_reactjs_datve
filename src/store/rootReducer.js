import { combineReducers } from "redux";

import { baiTapDatveReducerToolkit } from "./DatVe/slice";

export const rootReducer = combineReducers({
  //   reducer của redux tookit
  btDatveToolkit: baiTapDatveReducerToolkit,
});
